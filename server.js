var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log('Esperando que funcione Polymer desde node : ' + port);

app.get("/",function(req,res){
  //Se toma el html del build
  res.sendFile("index.html",{root:'.'});
})
